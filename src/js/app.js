import './plugins/import-jquery';
import documentReady from './helpers/documentReady';
import tabs from './components/tabs';
import accordion from './components/accordion';
import mobileNav from './components/mobile-nav';
import collapce from './components/collapse';
import Sticky from './components/sticky';
import intlTelInput from 'intl-tel-input';
import 'jquery-datetimepicker';
import '@fancyapps/fancybox'

import AOS from 'aos';
import Choices from 'choices.js';
import Swiper, {Autoplay, Navigation, Pagination} from 'swiper';

Swiper.use([Navigation, Pagination, Autoplay]);


documentReady(() => {
    mobileNav();
    tabs();
    accordion();
    collapce();
    Sticky();

    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);

    const selectElem = document.querySelector('.select-search');
    if (selectElem) {
        window.prettySelect = new Choices(selectElem);
    }


    const club = new Swiper('.club-slider', {
        spaceBetween: 40,
        loop: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        },
        navigation: {
            nextEl: '.club-next',
            prevEl: '.club-prev',
        }
    });

    window.inputPhone = document.querySelector("#phone");
    if (window.inputPhone) {
        window.intlTel=intlTelInput(window.inputPhone, {
            // allowDropdown: false,
            // autoHideDialCode: false,
            // autoPlaceholder: "off",
            // dropdownContainer: document.body,
            // excludeCountries: ["us"],
            formatOnDisplay: true,
            // geoIpLookup: function(callback) {
            //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
            //     var countryCode = (resp && resp.country) ? resp.country : "";
            //     callback(countryCode);
            //   });
            // },
            hiddenInput: "full_number",
            initialCountry: "us",
            // localizedCountries: { 'de': 'Deutschland' },
            nationalMode: true,
            // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
            placeholderNumberType: "MOBILE",
            // preferredCountries: ['cn', 'jp'],
            // separateDialCode: true,
            utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/utils.min.js",
        });
        //window.intlTelInputGlobals.loadUtils("https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/utils.js");
    }

    const promoSlider = new Swiper('.promo-slider', {
        spaceBetween: 0,
        loop: true,
        autoplay: {
            delay: 3000,
            disableOnInteraction: false,
        },
    });

    const promoTimer = document.querySelector('#my-timer');
    if (promoTimer) {

        function getTimeRemaining(endtime) {
            const total = Date.parse(endtime) - Date.parse(new Date());
            const seconds = Math.floor((total / 1000) % 60);
            const minutes = Math.floor((total / 1000 / 60) % 60);
            const hours = Math.floor((total / (1000 * 60 * 60)) % 24);
            const days = Math.floor(total / (1000 * 60 * 60 * 24));

            return {
                total,
                days,
                hours,
                minutes,
                seconds
            };
        }

        function initializeClock(id) {
            const clock = document.getElementById(id);
            const daysSpan = clock.querySelector('.timer-days');
            const hoursSpan = clock.querySelector('.timer-hours');
            const minutesSpan = clock.querySelector('.timer-minutes');
            const secondsSpan = clock.querySelector('.timer-seconds');
            const deadline = clock.getAttribute('data-timer');

            function updateClock() {
                const t = getTimeRemaining(deadline);

                daysSpan.innerHTML = t.days;
                hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
                minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
                secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

                if (t.total <= 0) {
                    clearInterval(timeinterval);
                }
            }

            updateClock();
            const timeinterval = setInterval(updateClock, 1000);
        }

        initializeClock('my-timer');
    }

    AOS.init();

    $('.lng__button').on('click', function(e){
        e.preventDefault();
        $('.lng').toggleClass('open');
    });

    $('.lng__dropdown a').on('click', function(){
        let lngName = $(this).html();
        $(".lng").removeClass('open');
        $('.lng__button--active').html(lngName);
    });

    const reviewSlider = new Swiper('.review-slider', {
        slidesPerView: "auto",
        spaceBetween: 20,
        navigation: {
            nextEl: '.review-next',
            prevEl: '.review-prev',
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        },
        breakpoints: {
            1200: {
                slidesPerView: 4,
                spaceBetween: 0
            }
        }
    })

    const showcaseSlider = new Swiper('.showcase-slider', {
        slidesPerView: "auto",
        spaceBetween: 20,
        navigation: {
            nextEl: '.showcase-next',
            prevEl: '.showcase-prev',
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        },
        breakpoints: {
            1200: {
                slidesPerView: 4,
                spaceBetween: 0
            }
        }
    })

    // Modal
    $('.btn-modal').fancybox({
        autoFocus: false,
    });

});

$(function() {
    $(".date-field").datetimepicker({
        timepicker:false,
        format:'Y-m-d'
    });
});





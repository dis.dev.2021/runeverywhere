export default () => {
    const pageNavToggle = document.querySelector('.nav-toggle');
    const pageContainer = document.querySelector('.root');
    const pageLayout = document.querySelector('.header__layout');

    if (pageNavToggle) {
        pageNavToggle.addEventListener('click', (e) => {
            pageContainer.classList.toggle('nav-open');
        });
        pageLayout.addEventListener('click', (e) => {
            pageContainer.classList.toggle('nav-open');
        });
    }
};
